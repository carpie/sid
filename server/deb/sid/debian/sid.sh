#!/bin/bash
#
# Starter script for sid
#
BASEDIR=/opt/sid
export PATH=${BASEDIR}/node/bin:$PATH
cd ${BASEDIR}
source .env
if [ ! -f "certs/sidserver.key" ]; then
    pushd certs
    ../scripts/mkcert --ca "SIDCA" --certname "sidserver" --days 3650 --ip $(hostname -I | awk '{ print $1 }')
    popd
fi
echo $PWD
node src/app.js
