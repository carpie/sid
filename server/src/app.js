'use strict';
require('dotenv').config();

const cookieParser = require('cookie-parser');
const express = require('express');
const fs = require('fs');
const https = require('https');

const auth = require('./auth.js');
const configManager = require('./configmanager.js');
const logger = require('./logger');
const syslogMonitor = require('./syslogmonitor.js');
logger.level = process.env.LOG_LEVEL;

const app = express();
app.use(cookieParser());
app.use(express.json());

// Auth handlers
app.get('/auth', auth.authCheckHandler);
app.post('/auth', auth.loginHandler);
app.delete('/auth', auth.logoutHandler);


// IP method handlers
app.get('/requests$', (req, res) => {
  res.json(syslogMonitor.getRequests());
});


app.post('/requests/:mac$', (req, res) => {
  logger.info('assigning address for %s as %s', req.params.mac, req.body.hostname);
  configManager.addMac(req.params.mac, req.body.hostname)
    .then((rec) => {
      syslogMonitor.removeRequest(req.params.mac);
      return res.json(rec);
    })
    .catch((err) => {
      logger.error('addMac failed: %s', err);
      return res.status(500).json({ error: err.toString() });
    });
});


app.delete('/requests/:mac$', (req, res) => {
  if (!syslogMonitor.isValidRequest(req.params.mac)) {
    return res.status(404).json({ error: 'Not found' });
  }
  logger.info('denying address for %s', req.params.mac);
  syslogMonitor.removeRequest(req.params.mac);
  return res.json({});
});

// Serve the client app on the root
app.use('/', express.static('static'));

const options = {
  cert: fs.readFileSync('certs/sidserver.crt'),
  key: fs.readFileSync('certs/sidserver.key')
};
https.createServer(options, app)
  .listen(process.env.PORT, () => {
    logger.info('SID listening on port %s...', process.env.PORT);
    logger.info('Log level is %s', logger.level);
    syslogMonitor.monitor();
  });
