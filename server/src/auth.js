// Authentication endpoints
'use strict';

const crypto = require('crypto');

const jwt = require('jsonwebtoken');

const logger = require('./logger');

const TOKEN_TIMEOUT_SEC = 10 * 60; // 10 minutes in seconds
const TOKEN_TIMEOUT_MS = TOKEN_TIMEOUT_SEC * 1000; // 10 minutes in milliseconds
const JWT_SECRET = crypto.randomBytes(16).toString('base64');

const cookieConfig = {
  httpOnly: true,
  secure: true,
  maxAge: TOKEN_TIMEOUT_MS
};

const authCheckHandler = (req, res) => {
  try {
    jwt.verify(req.cookies.sidjwt, JWT_SECRET);
    return res.status(204).send();
  } catch (err) {
    logger.info(`error in jwt verification: ${err}`);
  }

  return res.status(401).send({ error: 'Unauthorized' });
};

const loginHandler = (req, res) => {
  if (req.body.pin === process.env.SID_PIN) {
    const sidJwt = jwt.sign({ sub: 'SID' }, JWT_SECRET, { expiresIn: TOKEN_TIMEOUT_SEC });
    res.cookie('sidjwt', sidJwt, cookieConfig);
    return res.status(204).send();
  }
  return res.status(400).send({ error: 'Missing or bad PIN' });
};

const logoutHandler = (_, res) => {
  res.clearCookie('sidjwt');
  return res.status(204).send();
};

module.exports = {
  authCheckHandler,
  loginHandler,
  logoutHandler
};
