'use strict';

const winston = require('winston');

const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.splat(),
    winston.format.simple()
  ),
  transports: [
    new (winston.transports.Console)({
      timestamp: () => new Date().toISOString()
    })
  ]
});

module.exports = logger;
