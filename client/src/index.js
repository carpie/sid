import { inspect } from '@xstate/inspect';

import './style';
import App from './components/app';

if (process.env.NODE_ENV === 'development') {
  inspect({ iframe: false });
}
export default App;
