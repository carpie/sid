// styled theme
export const theme = {
  breakpoints: ['48rem', '90rem'],
  colors: {
    base: '#121212',
    overlays: [
      'rgba(255, 255, 255, .05)',
      'rgba(255, 255, 255, .07)',
      'rgba(255, 255, 255, .08)',
      'rgba(255, 255, 255, .09)',
      'rgba(255, 255, 255, .11)',
      'rgba(255, 255, 255, .12)',
      'rgba(255, 255, 255, .14)',
      'rgba(255, 255, 255, .15)',
      'rgba(255, 255, 255, .16)'
    ],
    // primary: '#b39ddb',
    // primary: '#90caf9',
    // primary: '#ce93d8',
    // primary: '#d64b72',
    // primary: '#cd486f',
    primary: '#fc72fd',

    secondary: '#80deea',
    highlight: '#90afc5',
    text: '#e2e2e2',
    darkText: '#111',
    headerText: '#e2e2e2',
    secondaryText: '#a2a2a2',
    transparent: 'transparent',
    translucentBackground: 'rgba(0, 0, 0, 0.8)',
    dialogBackground: '#2d2d2d',
    primaryButton: '#56969e'
  },
  fonts: {
    body: 'system-ui, sans-serif',
    heading: 'inherit'
  },
  fontSizes: ['.25rem', '.5rem', '.75rem', '1rem', '1.25rem', '1.5rem', '1.75rem', '2rem', '2.25rem', '2.5rem'],
  space: [0, '.25rem', '.5rem', '.75rem', '1rem', '2rem', '4rem', '8rem'],
  iconSizes: ['1rem', '2rem', '4rem', '6rem', '8rem', '10rem']
};

theme.colors.appBar = theme.colors.overlays[5];
theme.media = {
  medium: `@media (min-width: ${theme.breakpoints[0]})`,
  large: `@media (min-width: ${theme.breakpoints[1]})`
};
