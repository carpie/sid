import { h } from 'preact';

import { format, parseISO } from 'date-fns';
import styled from '@emotion/styled';

import * as t from '../style/helpers';
import { Button } from './Button';
import { Error } from './Error';
import { Header } from './Header';
import { Icon } from './Icon';
import { Loading } from './Loading';

const List = styled.ul`
  list-style: none;
  flex: 1;
  padding: 0;
  margin: ${t.spaces(1)} 0 0;
`;

const Li = styled.li`
  display: flex;
  align-items: center;
  padding: ${t.spaces(3)};
  background-color: ${p => p.theme.colors.overlays[1]};
  margin-bottom: ${t.spaces(1)};
`;

const Mac = styled.span`
  font-size: ${t.fontSizes(5)};
  margin-bottom: ${t.spaces(2)};
`;

const MacContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: ${t.spaces(4)};
`
;
const Timestamp = styled.span`
  font-size: ${t.fontSizes(2)};
  color: ${t.colors('secondaryText')};
`;


const ListItem = ({ macRecord, onClick }) => {
  const { ts, mac } = macRecord;
  return (
    <Li onClick={onClick}>
      <Icon color="secondary" icon="signal" />
      <MacContainer>
        <Mac>{mac}</Mac>
        <Timestamp>Req at: {format(parseISO(ts), 'yyyy-MM-dd H:mm:ss aa')}</Timestamp>
      </MacContainer>
    </Li>
  );
};

export const UnassignedMacView = ({ state, send }) => (
  <>
    <Header
      rightButton={
        <Button icon="refresh" onClick={() => send('UNASSIGNED_MAC_REFRESH')} data-testid="refresh" />
      }
    >SID - Unassigned MACs</Header>
    {state.matches('unassignedMacView.loading') && <Loading />}
    {state.matches('unassignedMacView.idle') && (
      <List>
        {state.context.addressRequests.map(rec => (
          <ListItem key={rec.ts} macRecord={rec} onClick={() => send({ type: 'SELECT_MAC', macRecord: rec })} />
        ))}
      </List>
    )}
    {state.matches('unassignedMacView.error') && <Error>{state.context?.error}</Error>}
  </>
);
