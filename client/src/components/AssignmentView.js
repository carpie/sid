import { h } from 'preact';

import styled from '@emotion/styled';

import * as t from '../style/helpers';
import { Button } from './Button';
import { Error } from './Error';
import { Header } from './Header';
import { Input } from './Input';

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: ${t.spaces(5)};
  
  > button {
    margin-left: ${t.spaces(2)};
  }
`;

const Container = styled.section`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: ${t.spaces(5)} ${t.spaces(4)};
  font-size: ${t.fontSizes(4)};
`;

const Info = styled.span`
  color: ${t.colors('secondaryText')};
  margin-bottom: ${t.spaces(2)};
`;

const Mac = styled.span`
  font-size: ${t.fontSizes(5)};
  margin-bottom: ${t.spaces(5)};
`;

export const AssignmentView = ({ state, send }) => {
  const { error, hostname, selectedMac } = state.context;
  return (
    <>
      <Header leftButton={
        <Button icon="leftArrow" onClick={() => send('GO_BACK')} data-testid="back" />
      }>SID - Assign IP?</Header>
      <Container>
        { error && <Error>{error}</Error> }
        <Info>Device requesting IP</Info>
        <Mac>{selectedMac.mac}</Mac>
        <Info>Hostname (optional)</Info>
        <Input value={hostname} onChange={event => send({ type: 'UPDATE_HOSTNAME', value: event.target.value }) } />
        <ButtonContainer>
          <Button onClick={() => send('ASSIGN_IP')} data-testid="assign">Assign</Button>
        </ButtonContainer>
      </Container>
    </>
  );
};
