import { h } from 'preact';

import styled from '@emotion/styled';

import * as t from '../style/helpers';
import { Button } from './Button';
import { Error } from './Error';
import { Header } from './Header';
import { Input } from './Input';

const PinEntry = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: ${t.fontSizes(5)};
  margin-top: -${t.spaces(6)};
  
  > * {
    margin: ${t.spaces(4)}
  }
`;

export const AuthView = ({ state, send }) => (
  <>
    <Header>SID - Sign in</Header>
    {state.context.error && <Error>{state.context.error}</Error>}
    <PinEntry>
      <span>Enter PIN</span>
      <Input
        value={state.context.authPin}
        onChange={event => send({ type: 'PIN_CHANGED', value: event.target.value })}
        type="password"
        data-testid="password"
      />
      <Button onClick={() => send('SIGN_IN')} data-testid="signIn">Sign In</Button>
    </PinEntry>
  </>
);
