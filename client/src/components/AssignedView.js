import { h } from 'preact';

import styled from '@emotion/styled';

import * as t from '../style/helpers';
import { Button } from './Button';
import { Header } from './Header';

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: ${t.spaces(5)};

  > button {
    margin-left: ${t.spaces(2)};
  }
`;

const Container = styled.section`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: ${t.spaces(5)} ${t.spaces(4)};
  font-size: ${t.fontSizes(4)};
`;

const Label = styled.span`
  color: ${t.colors('secondaryText')};
  margin-bottom: ${t.spaces(2)};
`;

const Info = styled.span`
  font-size: ${t.fontSizes(5)};
  margin-bottom: ${t.spaces(5)};
`;

export const AssignedView = ({ state, send }) => {
  const { hostname, ip, mac } = state.context.dns;
  return (
    <>
      <Header
        leftButton={<Button icon="leftArrow" onClick={() => send('GO_BACK')} data-testid="back" />}
      >
        SID - Address assigned
      </Header>
      <Container>
        <Label>MAC</Label>
        <Info>{mac}</Info>
        <Label>IP</Label>
        <Info>{ip}</Info>
        <Label>Hostname</Label>
        <Info>{hostname}</Info>
        <ButtonContainer>
          <Button onClick={() => send('GO_BACK')} data-testid="ok">OK</Button>
        </ButtonContainer>
      </Container>
    </>
  );
};

