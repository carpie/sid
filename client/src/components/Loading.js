import styled from '@emotion/styled';

import { RotatingIcon } from './Icon';

// import * as t from '../style/helpers';

const LoadingContainer = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Loading = () => (
  <LoadingContainer>
    <RotatingIcon icon="loading" size="2" />
  </LoadingContainer>
);
