import { keyframes } from '@emotion/react';
import styled from '@emotion/styled';

import * as t from '../style/helpers';

import checkIcon from './check.svg';
import closeIcon from './circle-with-cross.svg';
import keyIcon from './key.svg';
import leftArrowIcon from './arrow-bold-left.svg';
import loadingIcon from './circular-graph.svg';
import refreshIcon from './cycle.svg';
import signalIcon from './signal.svg';

const nameToIconMap = {
  check: checkIcon,
  close: closeIcon,
  key: keyIcon,
  leftArrow: leftArrowIcon,
  loading: loadingIcon,
  refresh: refreshIcon,
  signal: signalIcon
};

export const Icon = styled.span`
  display: block;
  height: ${p => (typeof p.size === 'undefined') ? '2rem' : p.theme.iconSizes[p.size]};
  width: ${p => (typeof p.size === 'undefined') ? '2rem' : p.theme.iconSizes[p.size]};
  background-color: ${p => p.color ? p.theme.colors[p.color] : p.theme.colors.text};
  mask: ${p => `url(${nameToIconMap[p.icon] ?? closeIcon})`};
  mask-repeat: no-repeat;
  mask-origin: content-box;
  mask-size: cover;
  padding: ${t.spaces(1)};
`;

const spin = keyframes`
  0% {
    transform: rotate(0deg);
  },
  100% {
    transform: rotate(360deg);
  }
`;
export const RotatingIcon = styled(Icon)`
  animation: ${spin} 1s infinite;
`;
