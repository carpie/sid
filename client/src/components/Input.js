import styled from '@emotion/styled';

import * as t from '../style/helpers';

export const Input = styled.input`
  font-size: ${t.fontSizes(4)};
  padding: ${t.spaces(2)}; 
  background-color: ${t.colors('dialogBackground')};
  color: ${t.colors('text')};
  border: 1px ${t.colors('text')} solid;
`;
