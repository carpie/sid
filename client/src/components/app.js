import { h } from 'preact';

import { ThemeProvider } from '@emotion/react';
import styled from '@emotion/styled';

import { useMachine } from '@xstate/react';

import { sidMachine } from '../machines/sidMachine';
import * as t from '../style/helpers';
import { theme } from '../theme';
import { AssignedView } from './AssignedView';
import { AssignmentView } from './AssignmentView';
import { AuthView } from './AuthView';
import { UnassignedMacView } from './UnassignedMacView';

const Main = styled.main`
  display:flex;
  flex-direction: column;
  height: 100vh;
  color: ${t.colors('text')};
`;

const App = () => {
  const [state, send] = useMachine(sidMachine, { devTools: process.env.NODE_ENV === 'development' });
  return (
    <ThemeProvider theme={theme}>
      <Main>
        {state.matches('authView') && <AuthView state={state} send={send} />}
        {state.matches('unassignedMacView') && <UnassignedMacView state={state} send={send} />}
        {state.matches('assignmentView') && <AssignmentView state={state} send={send} />}
        {state.matches('assignedView') && <AssignedView state={state} send={send} />}
      </Main>
    </ThemeProvider>
  );
}

export default App;
