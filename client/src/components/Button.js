import styled from '@emotion/styled';

import * as t from '../style/helpers';

import { Icon } from './Icon';

export const StyledButton = styled.button`
  font-size: ${t.fontSizes(4)};
  padding: ${p => p.icon ? p.theme.space[1] : `${p.theme.space[2]} ${p.theme.space[4]}`};
  background-color: ${t.colors('primaryButton')};
  border-width: 0;
  color: ${t.colors('text')};
  border-radius: 4px;
`;

export const Button = ({ children, icon, ...props }) => {
  if (icon) {
    return (
      <StyledButton icon={icon} {...props}>
        <Icon icon={icon} />
        {children}
      </StyledButton>
    );
  }
  return <StyledButton {...props}>{children}</StyledButton>;
};
