import styled from '@emotion/styled';

import * as t from '../style/helpers';

export const Error = styled.div`
  border: 1px solid red;
  margin-bottom: ${t.spaces(4)};
  padding: ${t.spaces(4)};
  color: red;
  font-size: ${t.fontSizes(3)};
`;
