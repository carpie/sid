import styled from '@emotion/styled';

import * as t from '../style/helpers';

const StyledHeader = styled.header`
  background-color: ${t.colors('appBar')};
  color: ${t.colors('headerText')};
  font-size: ${t.fontSizes(4)};
  padding: ${t.spaces(2)};
  display: flex;
  align-items: center;
`;

const LeftContainer = styled.div`
  margin-right: ${t.spaces(2)};
`;

const RightContainer = styled.div`
  margin-left: auto;
`;

export const Header = ({ children, leftButton, rightButton }) => {
  return (
    <StyledHeader>
      {leftButton && (
        <LeftContainer>
          {leftButton}
        </LeftContainer>
      )}
      {children}
      {rightButton && (
        <RightContainer>
          {rightButton}
        </RightContainer>
      )}
    </StyledHeader>
  );
};
