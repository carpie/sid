export const colors = color => p => p.theme.colors[color];

export const fontSizes = fontSize => p => p.theme.fontSizes[fontSize];

export const spaces = space => p => p.theme.space[space];
