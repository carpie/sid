const fetchData = async (...args) => {
  console.log('fetchData args', args);
  const resp = await fetch(...args);
  const isJson = (resp.headers.get('Content-Type') ?? '').includes('application/json');
  const data = isJson ? await resp.json() : await resp.text();
  if (!resp.ok) {
    const msg = isJson ? data.error : data;
    const error = new Error(msg);
    error.status = resp.status;
    error.statusText = resp.statusText;
    throw error;
  }
  return data;
};

const postJson = (url, data, options) => (
  fetchData(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
    ...options
  })
);

export const checkAuth = () => fetchData('/auth');

export const requestIp = ({ hostname, selectedMac: { mac } }) => postJson(`/requests/${mac}`, { hostname });

export const getAddressRequests = () => fetchData('/requests');

export const login = ({ authPin }) => postJson('/auth', { pin: authPin });
