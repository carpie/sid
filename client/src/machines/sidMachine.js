import { assign, createMachine } from 'xstate';

import { checkAuth, requestIp, getAddressRequests, login } from './sidServices';

export const sidMachine = createMachine({
  id: 'sidMachine',
  context: {
    authPin: '',
    addressRequests: [],
    hostname: '',
    dns: {},
    selectedMac: null
  },
  initial: 'checkAuth',
  states: {
    checkAuth: {
      invoke: {
        src: 'checkAuth',
        onDone: 'unassignedMacView',
        onError: 'authView'
      }
    },
    authView: {
      initial: 'awaitingInput',
      states: {
        awaitingInput: {
          on: {
            PIN_CHANGED: {
              actions: 'updatePin'
            },
            SIGN_IN: 'loggingIn'
          }
        },
        loggingIn: {
          invoke: {
            src: 'login',
            onDone: {
              target: '#sidMachine.unassignedMacView',
              actions: 'clearPin'
            },
            onError: {
              target: 'awaitingInput',
              actions: 'assignError'
            }
          }
        }
      }
    },
    unassignedMacView: {
      initial: 'loading',
      states: {
        loading: {
          entry: 'clearError',
          invoke: {
            src: 'getAddressRequests',
            onDone: {
              target: 'idle',
              actions: 'assignAddressRequests'
            },
            onError: {
              target: 'error',
              actions: 'assignError'
            }
          }
        },
        idle: {
          on: {
            SELECT_MAC: {
              target: '#sidMachine.assignmentView',
              actions: 'assignSelectedMac'
            },
            UNASSIGNED_MAC_REFRESH: 'loading'
          }
        },
        error: {
          on: {
            UNASSIGNED_MAC_REFRESH: {
              target: 'loading',
              actions: 'clearError'
            }
          }
        }
      }
    },
    assignmentView: {
      initial: 'awaitingInput',
      states: {
        awaitingInput: {
          on: {
            ASSIGN_IP: 'requestingIp',
            GO_BACK: '#sidMachine.unassignedMacView',
            UPDATE_HOSTNAME: {
              actions: 'assignHostName'
            }
          }
        },
        requestingIp: {
          invoke: {
            src: 'requestIp',
            onDone: {
              target: '#sidMachine.assignedView',
              actions: ['clearError', 'assignDnsRecord']
            },
            onError: {
              target: 'awaitingInput',
              actions: 'assignError'
            }
          }
        }
      }
    },
    assignedView: {
      on: {
        GO_BACK: 'unassignedMacView'
      }
    }
  }
}, {
  actions: {
    assignError: assign({ error: (_, event) => event.data.message }),
    assignAddressRequests: assign({ addressRequests: (_, event) => { console.log('e', event); return event.data; } }),
    assignDnsRecord: assign({ dns: (_, event) => event.data }),
    assignHostName: assign({ hostname: (_, event) => event.value }),
    assignSelectedMac: assign({ selectedMac: (_, event) => event.macRecord }),
    clearError: assign({ error: '' }),
    clearPin: assign({ authPin: '' }),
    updatePin: assign({
      authPin: (_, event) => event.value
    })
  },
  services: {
    checkAuth,
    getAddressRequests,
    login,
    requestIp
  }
});
