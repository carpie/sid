/* globals cy */
describe('SidUI', () => {
  beforeEach(() => {
    cy.intercept('/requests', (() => {
      let callCount = 0;
      const res0 = [];
      const res1 = [
        {
          ts: '2021-08-04T23:29:57.309Z',
          mac: 'ac:88:fd:90:62:ef'
        },
        {
          ts: '2021-08-04T23:30:01.309Z',
          mac: 'ac:88:fd:90:62:aa'
        }
      ];
      return req => {
        req.reply({
          statusCode: 200,
          body: callCount === 0 ? res0 : res1
        });
        callCount += 1;
      };
    })()).as('getRequests')
  });

  it('succeeds on good logins', () => {
    cy.intercept('/auth', (() => {
      let callCount = 0;
      return req => {
        console.log('authcallCount', callCount);
        if (callCount === 0) {
          callCount += 1;
          req.reply({
            statusCode: 401
          });
          return;
        }
        req.reply({
          statusCode: 204
        });
      };
    })());
    cy.visit('http://localhost:8080');
    cy.contains(/Enter PIN/i).should('be.visible');
    cy.get('input').type('1234');
    cy.get('[data-testid=signIn]').click();
    cy.contains(/sid - unassigned macs/i).should('be.visible');
  });

  it('shows an error on bad logins', () => {
    cy.intercept('/auth', { statusCode: 401 });
    cy.intercept('POST', '/auth', {
      statusCode: 400,
      body: { error: 'Missing or bad PIN' }
    });
    cy.visit('http://localhost:8080');
    cy.contains(/Enter PIN/i).should('be.visible');
    cy.get('input').type('nope');
    cy.get('[data-testid=signIn]').click();
    cy.contains(/missing or bad pin/i).should('be.visible');
  });

  it('can assign an address to a MAC request', () => {
    cy.intercept('/auth', { statusCode: 204 });
    cy.intercept('POST', '/requests/ac:88:fd:90:62:aa', req => {
      expect(req.body).to.deep.equal({ hostname: 'myhostname' });
      req.reply({
        statusCode: 200,
        body: {
          mac: 'ac:88:fd:90:62:aa',
          ip: '192.168.0.42',
          hostname: 'myhostname'
        }
      });
    });
    cy.visit('http://localhost:8080');
    cy.contains(/unassigned MACs/i).should('be.visible');
    // No records should show
    cy.wait('@getRequests');
    cy.contains(/ac:88:fd:90:62:ef/i).should('not.exist');
    cy.contains(/ac:88:fd:90:62:aa/i).should('not.exist');
    // Refreshing the list loads records
    cy.get('[data-testid=refresh]').click();
    cy.contains(/ac:88:fd:90:62:ef/i).should('be.visible');
    cy.contains(/ac:88:fd:90:62:aa/i).should('be.visible').click();
    // Assign IP screen should open
    cy.contains(/assign ip\/?/i).should('be.visible');
    cy.contains(/ac:88:fd:90:62:aa/i).should('be.visible');
    cy.get('input').type('myhostname');
    cy.get('[data-testid=assign]').click();
    // Should show the results of the assignment
    cy.contains(/address assigned/i).should('be.visible');
    cy.contains(/ac:88:fd:90:62:aa/i).should('be.visible');
    cy.contains('192.168.0.42').should('be.visible');
    cy.contains(/myhostname/i).should('be.visible');
    cy.get('[data-testid=ok]').click();
    // Should go back to unassigned MAC screen
    cy.contains(/unassigned MACs/i).should('be.visible');
  });

  it('shows an error on a bad assignment', () => {
    cy.intercept('/auth', { statusCode: 204 });
    cy.intercept('POST', '/requests/ac:88:fd:90:62:aa', {
      statusCode: 400,
      body: { error: 'Error: MAC is already assigned an address' }
    });

    cy.visit('http://localhost:8080');
    cy.contains(/unassigned MACs/i).should('be.visible');
    // No records should show
    cy.wait('@getRequests');
    cy.contains(/ac:88:fd:90:62:ef/i).should('not.exist');
    cy.contains(/ac:88:fd:90:62:aa/i).should('not.exist');
    // Refreshing the list loads records
    cy.get('[data-testid=refresh]').click();
    cy.contains(/ac:88:fd:90:62:ef/i).should('be.visible');
    cy.contains(/ac:88:fd:90:62:aa/i).should('be.visible').click();
    // Assign IP screen should open
    cy.contains(/assign ip\/?/i).should('be.visible');
    cy.contains(/ac:88:fd:90:62:aa/i).should('be.visible');
    cy.get('input').type('myhostname');
    cy.get('[data-testid=assign]').click();
    // Should show the error
    cy.contains(/MAC is already assigned/i).should('be.visible');
    // Back button should go back
    cy.get('[data-testid=back]').click();
    // Should go back to unassigned MAC screen
    cy.contains(/unassigned MACs/i).should('be.visible');
  });
});
