module.exports = function(config) {
  if (config.devServer) {
    config.devServer.proxy = [
      {
        path: '/auth**',
        target: 'https://localhost:4253',
        secure: false
      },
      {
        path: '/request**',
        target: 'https://localhost:4253',
        secure: false
      },
      {
        path: '/requests/**',
        target: 'https://localhost:4253',
        secure: false
      }
    ];
  }
};
